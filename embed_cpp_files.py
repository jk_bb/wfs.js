#!/usr/bin/python
import subprocess

inputs = ["./dist/wfs.js", "./dist/wfs.js.map", './instalod/index.html', './instalod/app.js']

outputs = []
for inp in inputs:
    outputs.append(subprocess.Popen(["xxd", "-i", inp], stdout=subprocess.PIPE).communicate()[0])

outputData = ""

with open('cpp/HTMLSources.cpp.pre', 'r') as file:
    outputData += file.read()

for output in outputs:
    outputData += output

with open('cpp/HTMLSources.cpp.post', 'r') as file:
    outputData += file.read()

print(outputData)