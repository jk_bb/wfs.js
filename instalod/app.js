let global = {
  current_scaling_mode: "fit",
  wfs: undefined,
  lastTime: -1,
  videoElement: undefined,
  socket: undefined,
  socketOpen: false
};

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////

function getWidth() {
  var w =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;
  return w;
}

function getHeight() {
  var h =
    window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;
  return h;
}

function setScalingFull() {
  global.current_scaling_mode = "full";
}

function setScalingFit() {
  global.current_scaling_mode = "fit";
}

function getCanvasSize(videoWidth, videoHeight, scalingMode) {
  if (scalingMode == "full") {
    return [Math.floor(videoWidth), Math.floor(videoHeight)];
  }

  const minSize = 100;

  const canvasXOffset =
    20 +
    window.scrollX +
    document.querySelector("#canvas").getBoundingClientRect().left;
  const canvasYOffset =
    20 +
    window.scrollY +
    document.querySelector("#canvas").getBoundingClientRect().top;

  const quotient_width =
    Math.max(getWidth() - canvasXOffset, minSize) / videoWidth;
  const quotient_height =
    Math.max(getHeight() - canvasYOffset, minSize) / videoHeight;
  const quotient = Math.min(1.0, quotient_width, quotient_height);
  const width = quotient * videoWidth;
  const height = quotient * videoHeight;

  return [Math.floor(width), Math.floor(height)];
}

function videoToCanvas() {
  try {
    if (!global.wfs || global.wfs.rebuildNeeded()) {
      if (global.wfs) {
        global.wfs.destroy();
      }
      global.wfs = new Wfs();
      global.wfs.attachMedia(global.videoElement, "ch1");
      global.videoElement.play();
    }

    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d", { alpha: false });
    var time = global.videoElement.currentTime;
    if (
      time !== global.lastTime &&
      !global.videoElement.paused &&
      !global.videoElement.ended
    ) {
      var size = getCanvasSize(
        global.videoElement.videoWidth,
        global.videoElement.videoHeight,
        global.current_scaling_mode
      );
      canvas.width = size[0];
      canvas.height = size[1];
      ctx.drawImage(global.videoElement, 0, 0, size[0], size[1]);
      global.lastTime = time;
    }
    requestAnimationFrame(videoToCanvas);
  } catch (error) {
    console.error(error);
  }
}

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////

function createSocket() {
  var socket = new WebSocket('ws://' + window.location.host + '/communication');

  socket.onopen = function(e) {
    global.socketOpen = true;
  };

  socket.onmessage = function(event) {};

  socket.onclose = function(event) {
    if (event.wasClean) {
    } else {
      // e.g. server process killed or network down
      // event.code is usually 1006 in this case
      //  alert('[close] Connection died');
    }
    global.socketOpen = false;
  };

  socket.onerror = function(error) {};

  return socket;
}

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////

function zoomEvent(eventType) {
  if (!global.socketOpen) {
    return;
  }

  global.socket.send(
    JSON.stringify({
      eventType: eventType
    })
  );
}

function zoomIn() {
  zoomEvent("zoom_in");
}

function zoomOut() {
  zoomEvent("zoom_out");
}

function get_checkbox_checked(name) {
  var selectorString = 'input[name="' + name + '"]';

  var selector = document.querySelector(selectorString);

  if (selector === null) {
    return false;
  }

  if (selector.type === "checkbox") {
    return selector.checked;
  }

  return false;
}

function get_radio_value(name, defaultValue) {
  var selector = document.querySelector('input[name="' + name + '"]:checked');

  if (selector === null) {
    return defaultValue;
  }

  return selector.value;
}

function updateSettings() {
  if (!global.socketOpen) {
    return;
  }

  global.socket.send(
    JSON.stringify({
      eventType: "update_setting",
      mouseMode: get_radio_value("mouse_mode", "Click"),
      gizmoMode: get_radio_value("gizmo_mode", "None"),
      showStatistics: get_checkbox_checked("show_statistics"),
      drawMeshNormals: get_checkbox_checked("draw_mesh_normals"),
      highlightSelectedObjects: get_checkbox_checked(
        "highlight_selected_objects"
      ),
      showGrid: get_checkbox_checked("show_grid")
    })
  );
}

function handleMouse(event, eventType) {
  if (!global.socketOpen) {
    return;
  }
  var divPos = {};
  var box = $("#canvas");
  if (!box) {
    console.log("Canvas not found");
  }
  var left = event.pageX - box.offset().left;
  var top = event.pageY - box.offset().top;
  var width = box.width();
  var height = box.height();

  if (left >= 0 && top >= 0 && left <= width && top <= height) {
    var leftAdjusted = (global.videoElement.videoWidth * left) / width;
    var topAdjusted = (global.videoElement.videoHeight * top) / height;

    var mouseModeSelector = document.querySelector(
      'input[name="mouse_mode"]:checked'
    );
    var mouseMode = "Click";
    if (mouseModeSelector) {
      mouseMode = mouseModeSelector.value;
    }

    var gizmoModeSelector = document.querySelector(
      'input[name="gizmo_mode"]:checked'
    );
    var gizmoMode = "None";
    if (gizmoModeSelector) {
      gizmoMode = gizmoModeSelector.value;
    }

    global.socket.send(
      JSON.stringify({
        eventType: eventType,
        left: leftAdjusted,
        top: topAdjusted,
        mouseMode: mouseMode,
        gizmoMode: gizmoMode
      })
    );
  }
}

function handleKeyboard(event, eventType) {
  if (!global.socketOpen) {
    return;
  }
  global.socket.send(
    JSON.stringify({
      eventType: eventType,
      keyCode: event.keyCode,
      which: event.which
    })
  );
}

$(document).mousemove(function(e) {
  handleMouse(e, "mouse_move");
});
$(document).mousedown(function(e) {
  handleMouse(e, "mouse_down");
});
$(document).mouseup(function(e) {
  handleMouse(e, "mouse_up");
});
$(document).keyup(function(e) {
  handleKeyboard(e, "key_up");
});
$(document).keydown(function(e) {
  handleKeyboard(e, "key_down");
});
$(document).keypress(function(e) {
  handleKeyboard(e, "key_press");
});
